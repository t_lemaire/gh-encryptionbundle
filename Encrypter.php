<?php

namespace GorillaHub\EncryptionBundle;

class Encrypter {

	const IV_SEPARATOR     = '|';

	private $secret;
	private $algorithm;
	private $mode;

	function __construct($secret, $algorithm = MCRYPT_RIJNDAEL_256, $mode = MCRYPT_MODE_CBC) {

		$this->secret    = $secret;
		$this->algorithm = $algorithm;
		$this->mode      = $mode;
	}

	public function encrypt($data, $use_init_vector = true) {

		$init_vector    = $use_init_vector ? mcrypt_create_iv(mcrypt_get_iv_size($this->algorithm, $this->mode), MCRYPT_DEV_URANDOM) : null;
		$encrypted_data = base64_encode(mcrypt_encrypt($this->algorithm, $this->secret, $data, $this->mode, $init_vector));

		return $use_init_vector ? implode(self::IV_SEPARATOR, array(base64_encode($init_vector), $encrypted_data)) : $encrypted_data;
	}

	public function decrypt($encrypted_data) {

		$init_vector = null;

		if (false !== stripos($encrypted_data, self::IV_SEPARATOR)) {

			list($init_vector, $encrypted_data) = explode(self::IV_SEPARATOR, $encrypted_data, 2);
			$init_vector = base64_decode($init_vector);
		}

		$base64_decoded = base64_decode($encrypted_data, true);

		if ($base64_decoded) {

			$decrypted_data = trim(mcrypt_decrypt($this->algorithm, $this->secret, $base64_decoded, $this->mode, $init_vector));

			return $decrypted_data;
		} else {

			return $encrypted_data;
		}
	}
}
