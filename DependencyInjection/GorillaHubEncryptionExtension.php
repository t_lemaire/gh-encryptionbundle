<?php

namespace GorillaHub\EncryptionBundle\DependencyInjection;

use \Symfony\Component\Config\FileLocator;
use \Symfony\Component\DependencyInjection\ContainerBuilder;
use \Symfony\Component\DependencyInjection\Definition;
use \Symfony\Component\DependencyInjection\Loader;
use \Symfony\Component\DependencyInjection\Reference;
use \Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class GorillaHubEncryptionExtension extends Extension {

	/**
	 * {@inheritDoc}
	 */
	public function load(array $configs, ContainerBuilder $container) {

		$configuration = new Configuration();
		$config        = $this->processConfiguration($configuration, $configs);

		$loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
		$loader->load('services.yml');

		foreach ($config['encrypters'] as $name => $connectionConfiguration) {

			$container->setDefinition(sprintf('gorillahub_encryption.encrypter.%s', $name), new Definition(
				'GorillaHub\EncryptionBundle\Encrypter',
				array(
					$connectionConfiguration['secret']
				)
			));
		}
	}
}
