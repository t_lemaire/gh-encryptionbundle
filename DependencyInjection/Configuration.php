<?php

namespace GorillaHub\EncryptionBundle\DependencyInjection;

use \Symfony\Component\Config\Definition\Builder\TreeBuilder;
use \Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface {

	/**
	 * {@inheritDoc}
	 */
	public function getConfigTreeBuilder() {
        $treeBuilder = new TreeBuilder('gorillahub_encryption');
        if (method_exists($treeBuilder, 'getRootNode')) {
            $rootNode = $treeBuilder->getRootNode();
        } else {
            // BC for symfony/config < 4.2
            $rootNode = $treeBuilder->root('gorillahub_encryption');
        }

		$rootNode
			->children()
				->arrayNode('encrypters')
					->useAttributeAsKey('name')
					->prototype('array')
						->children()
							->scalarNode('secret')->end()
							->scalarNode('algorithm')->defaultValue('rijndael-256')->end()
							->scalarNode('mode')->defaultValue('cbc')->end()
						->end()
					->end()
				->end()
			->end();

		return $treeBuilder;
	}
}
